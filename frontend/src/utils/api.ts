import axios from "axios";

const BASE_URL = "https://komek2020.kz/app";

export const axiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 3000,
});
