import ReactGA from "react-ga";

type IAnalyticsData = {
  category: string;
  action: string;
};

export const trackEvent = (analyticsData: IAnalyticsData) => {
  const { category, action } = analyticsData;

  ReactGA.event({
    category,
    action,
  });
};
