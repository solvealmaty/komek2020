// TODO: Refactor to single component with OfferCard

import React, { useState } from "react";
import { Card, CardBody, CardTitle, CardSubtitle, CardText } from "reactstrap";

import { parseISO } from "date-fns";
import { format, zonedTimeToUtc } from "date-fns-tz";

interface Props {
  name: string;
  mobile: string;
  requirement: string;
  city: string;
  createdAt: string;
  trackPhoneClick: Function;
}

const TEXT_LIMIT = 50;

export const RequestCard = ({
  name,
  requirement,
  mobile,
  city,
  createdAt,
  trackPhoneClick,
}: Props) => {
  const [isExpanded, setIsExpanded] = useState(false);

  const localTime = zonedTimeToUtc(parseISO(createdAt), "GMT");

  return (
    <Card className="w-100">
      <CardBody className="d-flex flex-column">
        <CardText style={{ flex: 1 }}>
          {requirement.length > TEXT_LIMIT && !isExpanded
            ? requirement.substr(0, TEXT_LIMIT) + "..."
            : requirement}

          {!isExpanded && requirement.length > TEXT_LIMIT && (
            <button
              type="button"
              className="btn btn-link"
              onClick={() => setIsExpanded(true)}
            >
              Открыть полностью
            </button>
          )}
        </CardText>

        <CardTitle className="text-muted">
          {name}, {city}
          <br />
          <small>
            {format(localTime, "dd.MM.yyyy HH:mm", {
              timeZone: "Asia/Almaty",
            })}
          </small>
        </CardTitle>

        <CardSubtitle>
          <a
            onClick={() => trackPhoneClick()}
            className="btn btn-primary btn-sm"
            href={"tel:" + mobile}
          >
            {mobile}
          </a>
        </CardSubtitle>
      </CardBody>
    </Card>
  );
};
