import React from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

interface Props {
  currentPage: number;
  totalPages: number;

  onPageChange: (page: number) => void;
}

const createPaginationArray = (
  currentPage: number,
  totalPages: number
): number[] => {
  const SIDE_PAGE_ITEM_LIMIT = 3;

  const items = [];

  const leftItemCount =
    currentPage > SIDE_PAGE_ITEM_LIMIT ? SIDE_PAGE_ITEM_LIMIT : currentPage - 1;

  const rightItemCount =
    totalPages - currentPage > SIDE_PAGE_ITEM_LIMIT
      ? SIDE_PAGE_ITEM_LIMIT
      : totalPages - currentPage;

  const leftEdge = currentPage - leftItemCount;
  const rightEdge = currentPage + rightItemCount;

  for (let i = leftEdge; i <= rightEdge; i++) {
    items.push(i);
  }

  return items;
};

export const PaginationContainer = (props: Props) => {
  const goToPrevPage = () => {
    if (props.currentPage > 1) {
      props.onPageChange(props.currentPage - 1);
    }
  };
  const goToNextPage = () => {
    if (props.currentPage < props.totalPages) {
      props.onPageChange(props.currentPage + 1);
    }
  };

  return (
    <Pagination size="md" aria-label="Переход по страницам">
      <PaginationItem>
        <PaginationLink first href="#!" onClick={() => props.onPageChange(1)} />
      </PaginationItem>
      <PaginationItem>
        <PaginationLink previous href="#!" onClick={goToPrevPage} />
      </PaginationItem>

      {props.totalPages
        ? createPaginationArray(props.currentPage, props.totalPages).map(
            (item) => (
              <PaginationItem
                key={`pagination_${item}`}
                onClick={() => props.onPageChange(item)}
                active={item === props.currentPage}
              >
                <PaginationLink href="#!">{item}</PaginationLink>
              </PaginationItem>
            )
          )
        : null}

      <PaginationItem>
        <PaginationLink next href="#!" onClick={goToNextPage} />
      </PaginationItem>
      <PaginationItem>
        <PaginationLink
          last
          href="#!"
          onClick={() => props.onPageChange(props.totalPages)}
        />
      </PaginationItem>
    </Pagination>
  );
};
