import React from "react";
import { Switch, Route } from "react-router-dom";

import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import { HomePage } from "./pages/HomePage";
import { Request } from "./pages/Request";
import { Offer } from "./pages/Offer";
import { Offers } from "./pages/Offers";
import { Requests } from "./pages/Requests";

function App() {
  return (
    <>
      <Header />
      <main>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/requests">
            <Requests />
          </Route>
          <Route path="/request">
            <Request />
          </Route>
          <Route path="/offers">
            <Offers />
          </Route>
          <Route path="/offer">
            <Offer />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </main>
      <Footer />
    </>
  );
}

export default App;
