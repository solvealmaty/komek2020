import React, { useState, useEffect } from "react";

import { Container, Row, Col } from "reactstrap";

import PageHead from "../components/PageHead";
import { axiosInstance } from "../utils/api";
import { MedicationOfferItem } from "../types";
import useDebounce from "../utils/debounce-hook";
import { PaginationContainer } from "../components/PaginationContainer";
import { OfferCard } from "../components/OfferCard";
import { trackEvent } from "../utils/analytics";

const SEARCH_DEBOUNCE_TIME = 400;

export const Offers = () => {
  const [offers, setData] = useState<any>({
    content: [],
    pageable: {},
  });
  const [searchValue, setSearchValue] = useState("");
  const [citySearchValue, setCitySearchValue] = useState("");
  const [page, setPage] = useState(1);

  const debouncedSearchValue = useDebounce(searchValue, SEARCH_DEBOUNCE_TIME);
  const debouncedCitySearchValue = useDebounce(
    citySearchValue,
    SEARCH_DEBOUNCE_TIME
  );

  useEffect(() => {
    const fetchData = async () => {
      const result = await axiosInstance("/offer/list", {
        params: {
          query: debouncedSearchValue,
          city: debouncedCitySearchValue,
          page: page,
          pageSize: 20,
        },
      });

      setData(result.data);
    };

    fetchData();
  }, [page, debouncedSearchValue, debouncedCitySearchValue]);

  const handlePhoneClick = () => {
    trackEvent({
      category: "Offers",
      action: "User clicked phone button",
    });
  };

  return (
    <Container>
      <PageHead title="Список предложений помощи" />
      <Row className={"justify-content-center"}>
        <Col md={6}>
          <h1 className={"text-center mb-5"}>Список предложений</h1>
        </Col>
      </Row>
      <Row className={"justify-content-right"}>
        <Col md={4}>
          <div className="form-group">
            <label htmlFor="cityInput">Город</label>

            <input
              id="cityInput"
              type="text"
              className="form-control"
              aria-describedby="Город"
              value={citySearchValue}
              onChange={(e) => setCitySearchValue(e.target.value)}
            />
          </div>
        </Col>
        <Col md={4}>
          <div className="form-group">
            <label htmlFor="searchInput">Что ищем</label>

            <input
              id="searchInput"
              type="text"
              className="form-control"
              aria-describedby="Что ищем"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
            />
          </div>
        </Col>
      </Row>
      <Row>
        {offers.content.map((offer: MedicationOfferItem) => (
          <Col
            xs={12}
            lg={3}
            md={4}
            className="d-flex align-items-stretch mb-3"
            key={`offer_${offer.id}`}
          >
            <OfferCard {...offer} trackPhoneClick={handlePhoneClick} />
          </Col>
        ))}

        <Col
          xs={12}
          className="mt-3 px-xs-1 px-md-3 d-flex justify-content-center"
        >
          <PaginationContainer
            currentPage={page}
            totalPages={offers.totalPages}
            onPageChange={(newPage) => setPage(newPage)}
          />
        </Col>
      </Row>
    </Container>
  );
};
