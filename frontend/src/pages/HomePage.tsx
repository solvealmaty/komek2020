import React from "react";

import { Link } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardFooter,
  Button,
  Alert,
} from "reactstrap";

import { routes } from "../routes";
import PageHead from "../components/PageHead";
import { OutboundLink } from "react-ga";

export const HomePage = () => {
  return (
    <Container>
      <PageHead title="Komek 2020 - Важен вклад каждого!" />
      <Row className={"justify-content-center"}>
        <Col md={6}>
          <h1 className={"text-center"}>Важен вклад каждого!</h1>
          <p className={"text-center"}>
            Мы призываем включится всех и создавать каждому вокруг себя очаги
            помощи. В этой ситуации находятся все без исключения, поэтому важна
            инициатива каждого.
          </p>
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          <Alert color="dark">
            <div className="ribbon">
              <span>&nbsp;</span>
            </div>

            <h4>
              Мы глубоко соболезнуем всем родным и близким жертв пандемии
              COVID-19.
            </h4>
            <p>
              Призываем всех соблюдать меры безопасности и заботиться о своих
              близких.
            </p>
          </Alert>
          <Alert color="danger">
            Данный сайт предназначен для помощи и поддержки друг друга. <br />
            Пожалуйста, перед употреблением любого препарата -{" "}
            <strong>ОБРАТИТЕСЬ К ВРАЧУ.</strong> <br /> Не занимайтесь
            самолечением!
          </Alert>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col lg={3} md={6} sm={12} className="mb-3">
          <Card className="h-100">
            <CardBody>
              <CardTitle>
                <h2 className="h4">Мне нужна помощь</h2>
              </CardTitle>
              <ol className="card__list">
                <li>Нажмите на кнопку "Нужна помощь"</li>
                <li>
                  Заполните анкету <br />
                  Подробно распишите в чем вы нуждаетесь, какая помощь,
                  лекарства, аппараты вам необходимы
                </li>
              </ol>
            </CardBody>
            <CardFooter className="text-center">
              <Button color="primary" tag={Link} to={routes.request.path}>
                Заполнить
              </Button>
            </CardFooter>
          </Card>
        </Col>
        <Col lg={3} md={6} sm={12} className="mb-3">
          <Card className="h-100">
            <CardBody>
              <CardTitle>
                <h2 className="h4">Я хочу помочь</h2>
              </CardTitle>
              <ol className="card__list">
                <li>Нажмите на кнопку "Хочу помочь"</li>
                <li>
                  Заполните анкету <br />
                  Максимально развернуто укажите чем вы можете помочь:
                  лекарства, аппараты и любая другая форма помощи
                </li>
              </ol>
            </CardBody>
            <CardFooter className="text-center">
              <Button color="primary" tag={Link} to={routes.offer.path}>
                Заполнить
              </Button>
            </CardFooter>
          </Card>
        </Col>
        <Col lg={3} md={6} sm={12} className="mb-3">
          <Card className="h-100">
            <CardBody>
              <CardTitle>
                <h2 className="h4">Кто готов помочь</h2>
              </CardTitle>
              <CardText>
                В этом списке вы можете найти людей, которые могут чем-то
                помочь.
              </CardText>
            </CardBody>
            <CardFooter className="text-center">
              <Button color="primary" tag={Link} to={routes.offersList.path}>
                Посмотреть
              </Button>
            </CardFooter>
          </Card>
        </Col>
        <Col lg={3} md={6} sm={12} className="mb-3">
          <Card className="h-100">
            <CardBody>
              <CardTitle>
                <h2 className="h4">Кому нужна помощь</h2>
              </CardTitle>
              <CardText>
                В этом списке вы можете найти людей, которым нужна помощь.
              </CardText>
            </CardBody>
            <CardFooter className="text-center">
              <Button color="primary" tag={Link} to={routes.requestList.path}>
                Посмотреть
              </Button>
            </CardFooter>
          </Card>
        </Col>
        <Col lg={3} md={6} sm={12} className="mb-3">
          <Card className="h-100">
            <CardBody>
              <CardTitle>
                <h2 className="h4">Убрать мои данные с таблицы</h2>
              </CardTitle>
              <CardText>
                Если Вам уже помогли или у Вас закончились лекарства/аппараты и
                так далее, которыми вы могли поделиться, то заполните анкету -
                мы удалим вас из общей таблицы
              </CardText>
            </CardBody>
            <CardFooter className="text-center">
              <OutboundLink
                eventLabel="User clicked Delete From Table button"
                to="https://forms.gle/83rfxTghy8dLUmex9"
                target="_blank"
              >
                <Button color="primary">Перейти</Button>
              </OutboundLink>
            </CardFooter>
          </Card>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col lg={12}>
          <Alert color="warning">
            Это социальная площадка для обмена и помощью лекарственными
            препаратами, едой. Только обмен никакой коммерции. Мы не несём
            ответственность за медикаменты и любые операции между
            пользователями. <br /> Обязательно проверяйте сроки годности
            препаратов и обращайтесь к врачам!
          </Alert>
        </Col>
      </Row>
    </Container>
  );
};
