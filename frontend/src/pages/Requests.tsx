import React, { useState, useEffect } from "react";

import { Container, Row, Col } from "reactstrap";

import PageHead from "../components/PageHead";
import { axiosInstance } from "../utils/api";
import { MedicationRequestItem } from "../types";
import useDebounce from "../utils/debounce-hook";
import { PaginationContainer } from "../components/PaginationContainer";
import { RequestCard } from "../components/RequestCard";
import { trackEvent } from "../utils/analytics";

const SEARCH_DEBOUNCE_TIME = 400;

export const Requests = () => {
  const [requests, setData] = useState<any>({
    content: [],
    pageable: {},
  });
  const [searchValue, setSearchValue] = useState("");
  const [citySearchValue, setCitySearchValue] = useState("");
  const [page, setPage] = useState(1);

  const debouncedSearchValue = useDebounce(searchValue, SEARCH_DEBOUNCE_TIME);
  const debouncedCitySearchValue = useDebounce(
    citySearchValue,
    SEARCH_DEBOUNCE_TIME
  );

  useEffect(() => {
    const fetchData = async () => {
      const result = await axiosInstance("/request/list", {
        params: {
          query: debouncedSearchValue,
          city: debouncedCitySearchValue,
          page: page,
          pageSize: 20,
        },
      });

      setData(result.data);
    };

    fetchData();
  }, [page, debouncedSearchValue, debouncedCitySearchValue]);

  const handlePhoneClick = () => {
    trackEvent({
      category: "Requests",
      action: "User clicked phone button",
    });
  };

  return (
    <Container>
      <PageHead title="Список запросов о помощи" />
      <Row className={"justify-content-center"}>
        <Col md={6}>
          <h1 className={"text-center mb-5"}>Список запросов</h1>
        </Col>
      </Row>
      <Row className={"justify-content-right"}>
        <Col md={4}>
          <div className="form-group">
            <label htmlFor="cityInput">Город</label>

            <input
              id="cityInput"
              type="text"
              className="form-control"
              aria-describedby="Город"
              value={citySearchValue}
              onChange={(e) => setCitySearchValue(e.target.value)}
            />
          </div>
        </Col>
        <Col md={4}>
          <div className="form-group">
            <label htmlFor="searchInput">Что ищем</label>

            <input
              id="searchInput"
              type="text"
              className="form-control"
              aria-describedby="Что ищем"
              value={searchValue}
              onChange={(e) => setSearchValue(e.target.value)}
            />
          </div>
        </Col>
      </Row>
      <Row>
        {requests.content.map((request: MedicationRequestItem) => (
          <Col
            xs={12}
            lg={3}
            md={4}
            className="d-flex align-items-stretch mb-3"
            key={`request_${request.id}`}
          >
            <RequestCard {...request} trackPhoneClick={handlePhoneClick} />
          </Col>
        ))}

        <Col
          xs={12}
          className="mt-3 px-xs-1 px-md-3 d-flex justify-content-center"
        >
          <PaginationContainer
            currentPage={page}
            totalPages={requests.totalPages}
            onPageChange={(newPage) => setPage(newPage)}
          />
        </Col>
      </Row>
    </Container>
  );
};
