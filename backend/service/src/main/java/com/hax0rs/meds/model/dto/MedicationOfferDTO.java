package com.hax0rs.meds.model.dto;

import com.hax0rs.meds.model.StatusType;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
@Validated
public class MedicationOfferDTO {
    @ApiModelProperty(required = true)
    @NotNull
    private String name;
    @ApiModelProperty(required = true,
            allowableValues = "range[0, 20]")
    @NotNull
    @Size(min = 3, max = 15)
    private String mobile;
    @ApiModelProperty(required = true)
    @NotNull
    private String city;
    @NotNull
    private String offer;
}
