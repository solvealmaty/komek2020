package com.hax0rs.meds.service;


import com.hax0rs.meds.model.StatusType;
import com.hax0rs.meds.model.dto.EditDTO;
import com.hax0rs.meds.model.dto.MedicationOfferDTO;
import com.hax0rs.meds.model.dto.MedicationRequestDTO;
import com.hax0rs.meds.model.dto.PageFilterDTO;
import com.hax0rs.meds.model.entity.MedicationOffer;
import com.hax0rs.meds.model.entity.MedicationRequest;
import com.hax0rs.meds.repository.MedicationRequestRepository;
import com.hax0rs.meds.repository.MedicationOfferRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class MedsService {

    private final MedicationOfferRepository medicationOfferRepository;
    private final MedicationRequestRepository medicationRequestRepository;


    public void submitRequest(MedicationRequestDTO medicationRequestDTO) {
        MedicationRequest medicationRequest = MedicationRequest.builder()
                .city(medicationRequestDTO.getCity())
                .mobile(medicationRequestDTO.getMobile())
                .name(medicationRequestDTO.getName())
                .requirement(medicationRequestDTO.getRequirement())
                .status(StatusType.OPEN)
                .createdAt(LocalDateTime.now())
                .build();
        medicationRequestRepository.save(medicationRequest);
    }


    public void submitOffer(MedicationOfferDTO medicationOfferDTO) {
        MedicationOffer medicationRequest = MedicationOffer.builder()
                .city(medicationOfferDTO.getCity())
                .mobile(medicationOfferDTO.getMobile())
                .name(medicationOfferDTO.getName())
                .offer(medicationOfferDTO.getOffer())
                .status(StatusType.OPEN)
                .createdAt(LocalDateTime.now())
                .build();
        medicationOfferRepository.save(medicationRequest);
    }

    private Pageable getPageAble(PageFilterDTO pageFilter) {
        Sort sort = null;
        if (pageFilter.getSortBy() != null) {
            sort = Sort.by(pageFilter.getSortDirection(), pageFilter.getSortBy().toString());
        } else {
            sort = Sort.unsorted();
        }
        return PageRequest.of(pageFilter.getPage() - 1, pageFilter.getPageSize(), sort);
    }

    public Page<MedicationOffer> getOffers(PageFilterDTO pageFilter) {

        Pageable pageable = getPageAble(pageFilter);
        if (pageFilter.getQuery() != null && pageFilter.getCity() == null) {
            return medicationOfferRepository.findByOfferContainingIgnoreCaseAndStatus(pageFilter.getQuery(), StatusType.OPEN, pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() == null) {
            return medicationOfferRepository.findByCityContainingIgnoreCaseAndStatus(pageFilter.getCity(), StatusType.OPEN, pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() != null) {
            return medicationOfferRepository.findByCityContainingIgnoreCaseAndOfferContainingIgnoreCaseAndStatus(pageFilter.getCity(), pageFilter.getQuery(), StatusType.OPEN, pageable);
        }
        return medicationOfferRepository.findAllByStatus(StatusType.OPEN, pageable);
    }

    public void editOffer(EditDTO editDTO) {
        MedicationOffer medicationOffer = medicationOfferRepository.findOneById(editDTO.getId());
        if (medicationOffer == null) {
            throw new EntityNotFoundException();
        }
        medicationOffer.setStatus(editDTO.getStatus());
        medicationOffer.setReason(editDTO.getReason());
        medicationOfferRepository.save(medicationOffer);
    }


    public void editRequest(EditDTO editDTO) {
        MedicationRequest medicationRequest = medicationRequestRepository.findOneById(editDTO.getId());
        if (medicationRequest == null) {
            throw new EntityNotFoundException();
        }
        medicationRequest.setStatus(editDTO.getStatus());
        medicationRequest.setReason(editDTO.getReason());
        medicationRequestRepository.save(medicationRequest);
    }

    public Page<MedicationRequest> getRequests(PageFilterDTO pageFilter) {
        Pageable pageable = getPageAble(pageFilter);
        if (pageFilter.getQuery() != null && pageFilter.getCity() == null) {
            return medicationRequestRepository.findByRequirementContainingIgnoreCaseAndStatus(pageFilter.getQuery(), StatusType.OPEN, pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() == null) {
            return medicationRequestRepository.findByCityContainingIgnoreCaseAndStatus(pageFilter.getCity(), StatusType.OPEN, pageable);
        } else if (pageFilter.getCity() != null && pageFilter.getQuery() != null) {
            return medicationRequestRepository.findByCityContainingIgnoreCaseAndRequirementContainingIgnoreCaseAndStatus(pageFilter.getCity(), pageFilter.getQuery(), StatusType.OPEN, pageable);
        }
        return medicationRequestRepository.findAllByStatus(StatusType.OPEN, pageable);
    }
}
