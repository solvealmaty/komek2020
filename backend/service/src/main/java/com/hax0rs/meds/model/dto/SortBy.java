package com.hax0rs.meds.model.dto;

public enum SortBy {
    id,
    createdAt,
    city
}
