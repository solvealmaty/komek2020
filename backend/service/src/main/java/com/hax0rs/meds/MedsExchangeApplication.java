package com.hax0rs.meds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class MedsExchangeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedsExchangeApplication.class, args);
    }

}
