package com.hax0rs.meds.model.entity;

import com.hax0rs.meds.model.StatusType;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "medication_request")
@Data
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Builder
public class MedicationRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String mobile;
    private String city;
    private String requirement;
    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private StatusType status;

    private String reason;
}
